'use strict';

global.del          = require( 'del' );
global.crypto       = require( 'crypto' );
global.fs           = require( 'fs' );
global.gulp         = require( 'gulp' );
global.livereload   = require( 'gulp-livereload' );
global.plumber      = require( 'gulp-plumber' );
global.runsequence  = require( 'run-sequence' );

require( 'require-dir' )( './tasks' );
