'use strict';

const   cleanCss    = require( 'gulp-clean-css' ),
        concat      = require( 'gulp-concat' ),
        readlines   = require( 'readlines' ),
        replace     = require( 'gulp-replace' );

gulp.task( 'minifyStyles', () => {

    del( paths.distCss + '/' + pkg.name + '-*.css' );

    let lines                   = readlines.readlinesSync( project.header.file ),
        bowerAssetsStarted      = false,
        bowerAssetsFinished     = false,
        bowerAssetsFiles        = [],
        bowerAssetsToInject     = [],
        hasBowerMarkStartString = function( currentLine ) {
            return currentLine.indexOf( 'bower:css' ) > -1;
        },
        hasBowerMarkEndString   = function( currentLine ) {
            return currentLine.indexOf( 'endbower' ) > -1;
        };

    for ( let i in lines ) {
        let currentLine = lines[ i ];

        if ( hasBowerMarkStartString( currentLine ) ) {
            bowerAssetsStarted = true;
        }

        if ( hasBowerMarkEndString( currentLine ) ) {
            bowerAssetsFinished = true;
        }

        if ( bowerAssetsStarted && ! bowerAssetsFinished ) {
            bowerAssetsFiles.push( currentLine.trim() );
        }
    }

    bowerAssetsFiles.shift();

    for ( let i in bowerAssetsFiles ) {
        let currentPath = bowerAssetsFiles[ i ],
            regExp      = new RegExp( '\/bower_components\/(.*)\.css', 'gi' ),
            bowerPath   = currentPath.match( regExp );

        bowerAssetsToInject.push( paths.srcAssets + bowerPath.toString() );
    }

    gulp.src( bowerAssetsToInject.concat( [ paths.srcStylesheet + '/style.css' ] ) )
        .pipe( concat( pkg.name + '-' + RANDOM_STRING + '.css' ) )
        .pipe( cleanCss() )
        .pipe( replace( /url\(\.\.\/\.\.\/\.\.\/assets\//gi, 'url(../' ) )
        .pipe( gulp.dest( paths.distCss ) )
        .on( 'end', () => {
            runsequence( 'updateMainCssReference' );
        } );

} );
