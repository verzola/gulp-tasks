'use strict';

const wiredep = require( 'wiredep' ).stream;

gulp.task( 'wiredepScripts', () => {

    let supressSrcOnWiredeep = typeof project.supressSrcOnWiredeep !== 'undefined' ? project.supressSrcOnWiredeep : false,
        srcDefaultPath       = 'src';

    if ( supressSrcOnWiredeep ) {
        srcDefaultPath       = '';
    }

    let ignorePath  = (project.type == 'wordpress') ? /^(\/|\.+(?!\/[^\.]))+\.+/ : '',
        cssPath     = (project.type == 'wordpress') ? '<link rel="stylesheet" href="<?php echo bloginfo(\'template_url\') ?>/' + srcDefaultPath + '{{filePath}}">'    : '<link href="/{{filePath}}" rel="stylesheet">',
        jsPath      = (project.type == 'wordpress') ? '<script src="<?php echo bloginfo(\'template_url\') ?>/' + srcDefaultPath + '{{filePath}}"></script>'           : '<script src="/{{filePath}}"></script>';

    if (project.footer ) {
        gulp.src( [ project.footer.file ] )
            .pipe( wiredep( {
                ignorePath: ignorePath,
                fileTypes: {
                    html: {
                        replace: {
                            css : cssPath,
                            js  : jsPath
                        }
                    }
                }
            } ) )
            .pipe( gulp.dest( project.footer.path ) )
            .on( 'end', () => {
                runsequence( 'compileScripts' );
            } );
    } else {
        runsequence('compileScripts');
    }

} );
