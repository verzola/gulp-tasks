'use strict';

const   babel       = require( 'gulp-babel' ),
        concat      = require( 'gulp-concat' );

let babelify, browserify, source, stringify, vueify;

if ( project.type == 'vue' ) {
    babelify    = require( 'babelify' );
    browserify  = require( 'browserify' );
    source      = require( 'vinyl-source-stream' );
    stringify   = require( 'stringify' );
    vueify      = require( 'vueify' );
}

global.compileScriptsFlag = false;

gulp.task( 'compileScripts', () => {

    if ( paths.srcScript ) {
        gulp.src(scripts.toInject.concat(paths.srcScript + '/' + scripts.compiled))
            .pipe(concat(scripts.compiled, { newLine: "\n;" }))
            .pipe(plumber({
                handleError: err => {
                    console.log(err);
                    this.emit('end');
                }
            }))
            .pipe(babel({
                presets: ['es2015']
            }))
            .on('error', err => {
                console.log('>>> ERROR', err.message);
            })
            .pipe(gulp.dest(paths.srcScript))
            .on('end', () => {
                if (project.type == 'vue') {
                    browserify('./' + paths.srcScript + '/' + scripts.compiled)
                        .transform(babelify, {
                            presets: ["es2015"]
                        })
                        .transform(vueify)
                        .transform(stringify, {
                            appliesTo: { includeExtensions: ['.tpl'] }
                        })
                        .bundle()
                        .pipe(source(scripts.compiled))
                        .pipe(gulp.dest(paths.srcScript))
                        .pipe(livereload());
                } else {
                    gulp.src(paths.srcScript + '/' + scripts.compiled)
                        .pipe(gulp.dest(paths.srcScript))
                        .pipe(livereload());
                }

                if (!compileScriptsFlag) {
                    compileScriptsFlag = true;
                    runsequence('injectScripts');
                }
            });
    }

} );
