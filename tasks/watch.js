'use strict';

gulp.task( 'watch', () => {
    livereload.listen( 35729, err => {
        if ( err ) {
            return console.log( err );
        }
    } );

    runsequence( 'copyAssets' );

    gulp.watch( paths.srcStylesheet + '/**/*.{scss,sass}', [
        'compileStyles'
    ] );

    if ( paths.srcScript ) {
        if (project.type == 'vue') {
            gulp.watch([paths.srcScript + '/**/*.{tpl,vue}'], [
                'compileScripts'
            ]);
        }

        gulp.watch([paths.srcScript + '/**/*.js', '!' + paths.srcScript + '/script.js'], [
            'compileScripts'
        ]);
    }

} );
