'use strict';

const   replace     = require( 'gulp-replace' );

gulp.task( 'updateMainJsReference', () => {

    gulp.src( project.footer.file )
        .pipe( replace( /assets\/js\/(.*).js/gi, 'assets/js/' + pkg.name + '-' + RANDOM_STRING + '.js' ) )
        .pipe( gulp.dest( project.footer.path ) )
        .on( 'end', () => {
            runsequence( 'minifyStyles' );
        } );

} );
